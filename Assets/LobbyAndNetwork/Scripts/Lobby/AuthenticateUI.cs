using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AuthenticateUI : MonoBehaviour {


    [SerializeField] private Button authenticateButton;
    public bool AutoLogin = false;

    private void Awake() {
        authenticateButton.onClick.AddListener(() => {
            Login();
        });
    }

    private void Start()
    {
        if (AutoLogin)
        {
            Login();
        }
    }

    private void Login()
    {
        LobbyManager.Instance.Authenticate(EditPlayerName.Instance.GetPlayerName());
        Hide();
    }

    private void Hide() {
        gameObject.SetActive(false);
    }

}