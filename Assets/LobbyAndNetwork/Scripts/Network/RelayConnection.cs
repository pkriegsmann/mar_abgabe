using System.Threading.Tasks;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.Networking.Transport.Relay;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using UnityEngine;

public class RelayConnection : MonoBehaviour
{
    public static RelayConnection Instance { get; private set; }
    private const int MAXPLAYERS = 4;

    private void Awake()
    {
        Instance = this;
    }

    public async Task<string> CreateRelay()
    {
        try {
            Allocation allocation = await RelayService.Instance
                .CreateAllocationAsync(MAXPLAYERS);

            string joinCode = await RelayService.Instance
                .GetJoinCodeAsync(allocation.AllocationId);

            print("Joincode: " + joinCode);

            RelayServerData relayServerData = new(allocation, "dtls");

            NetworkManager.Singleton.GetComponent<UnityTransport>()
                .SetRelayServerData(relayServerData);

            NetworkManager.Singleton.StartHost();
            return joinCode;
        }
        catch(RelayServiceException e)
        {
            print(e);
            return null;
        }
    }

    public async void JoinRelay(string joinCode)
    {
        try
        {
            JoinAllocation joinAllocation = await RelayService.Instance.JoinAllocationAsync(joinCode);
            print("Join: " + joinCode);

            RelayServerData relayServerData = new(joinAllocation, "dtls");

            NetworkManager.Singleton.GetComponent<UnityTransport>()
                .SetRelayServerData(relayServerData);

            NetworkManager.Singleton.StartClient();
        }
        catch(RelayServiceException e)
        {
            print(e);
        }
    }

}
