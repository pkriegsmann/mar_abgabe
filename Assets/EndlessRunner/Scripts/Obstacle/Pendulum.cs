﻿using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class Pendulum : NetworkBehaviour
{
	public float speed = 1.5f;
	public float limit = 75f; //Limit in degrees of the movement
	public bool randomStart = false; //If you want to modify the start position
	private float random = 0;

	// Start is called before the first frame update
	void Awake()
    {
        if (!IsHost) { return; }
        if (randomStart)
			random = Random.Range(-0.5f, 0.5f);
	}

    // Update is called once per frame
    void Update()
    {
        if (!IsHost) { return; }
        float angle = limit * Mathf.Sin(Time.time + random * speed);
		transform.localRotation = Quaternion.Euler(0, 0, angle);
	}
}
