﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Unity.Netcode;
using UnityEngine;

public class Rotator : NetworkBehaviour
{
	private float _turnSpeed;
    private readonly float MINSPEED = 2.0f;
    private readonly float MAXPEED = 3.0f;

    private void Start()
    {
        if (!IsHost) { return; }
        float speed = Random.Range(MINSPEED, MAXPEED); 
        float direction = Random.value < 0.5f ? -1f : 1f;

        _turnSpeed = direction * speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsHost) { return; }
        transform.Rotate(0f , 0f, _turnSpeed * Time.deltaTime / 0.01f, Space.Self);
	}
}
