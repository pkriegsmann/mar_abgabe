using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class Obstacle : NetworkBehaviour
{
    [field: SerializeField] public float Force { get; private set; } = 10f; //Force 10000f
    [field: SerializeField] public float StunTime { get; private set; } = 0.5f;
    private readonly float _destroyBound = -3f;
    private Vector3 _flightDirection = Vector3.back;
    private readonly float _speed = 0.5f;

    void Start()
    {
        //_speed = Random.Range(1f, 3f);
        //_flightDirection.x = Random.Range(-0.2f, 0.2f);
        //transform.GetChild(0).GetComponent<Rigidbody>()
        //    .AddTorque(Random.Range(0, 100),
        //    Random.Range(0, 100), Random.Range(0, 100));
    }

    void Update()
    {
        if (!IsHost) { return; }
        transform.root.Translate(_speed * Time.deltaTime * _flightDirection);
        if (transform.position.z < _destroyBound)
        {
            transform.root.gameObject.GetComponent<NetworkObject>().Despawn();
            //Destroy(transform.root.gameObject);
        }
    }

}
