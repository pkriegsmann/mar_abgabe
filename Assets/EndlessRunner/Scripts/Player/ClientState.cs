using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientState : MonoBehaviour
{
    private PlayerController _playerController;

    void Start()
    {
        _playerController =
            (PlayerController)
            FindObjectOfType(typeof(PlayerController));
    }

    void OnGUI()
    {
        if (GUILayout.Button("A")) { }
        if (GUILayout.Button("A")) { }
        if (GUILayout.Button("A")) { }

        if (GUILayout.Button("Idle"))
            _playerController.Idle();

        if (GUILayout.Button("Start Flying"))
            _playerController.Moving();
    }
}
