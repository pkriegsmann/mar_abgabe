using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJumpState : MonoBehaviour, IPlayerState
{
    private PlayerController _playerController;
    [SerializeField] float jumpForce = 300f;
    Rigidbody _rb;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    public void Handle(PlayerController playerController)
    {
        if (!_playerController)
        {
            _playerController = playerController;
        }
        if (!_playerController.IsGrounded) { return; }
        _playerController.IsGrounded = false;
        _playerController.AnimController
            .SetBool(_playerController.JumpAnim, true);
        _playerController.PlayerState = PlayerStateType.JUMP;
        Jump();
    }

    public void UpdateState()
    {
    }

    private void Jump()
    {
        //_rb.velocity = new Vector3(_rb.velocity.x, jumpForce, _rb.velocity.z);
        _rb.AddForce(Vector3.up * jumpForce, ForceMode.Acceleration);
    }

    public void FixedUpdateState()
    {
    }
}
