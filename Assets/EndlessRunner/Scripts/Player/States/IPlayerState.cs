public interface IPlayerState
{
    void Handle(PlayerController controller);
    void UpdateState();
    void FixedUpdateState();
}

public enum PlayerStateType
{
    IDLE,
    MOVE,
    JUMP,
    STUN
}