using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveState : MonoBehaviour, IPlayerState
{
    private PlayerController _playerController;
    private readonly int Move = Animator.StringToHash("Move");

    public void Handle(PlayerController playerController)
    {
        if (!_playerController)
        {
            _playerController = playerController;
        }
        _playerController.AnimController.SetBool(_playerController.MoveAnim, true);
        _playerController.CurrentSpeed = _playerController.MaxSpeed;
        _playerController.PlayerState = PlayerStateType.MOVE;
    }

    public void UpdateState()
    {
    }

    public void FixedUpdateState()
    {
    }
}
