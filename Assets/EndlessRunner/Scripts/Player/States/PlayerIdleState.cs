using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIdleState : MonoBehaviour, IPlayerState
{
    private PlayerController _playerController;

    public void FixedUpdateState()
    {
    }

    public void Handle(PlayerController playerController)
    {
        if (!_playerController)
        {
            _playerController = playerController;
        }
        _playerController.PlayerState = PlayerStateType.IDLE;
        _playerController.AnimController.SetBool(_playerController.IdleAnim, true);
        _playerController.CurrentSpeed = 0f;
    }

    public void UpdateState()
    {
    }
}
