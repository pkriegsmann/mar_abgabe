using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStunState : MonoBehaviour, IPlayerState
{
    private PlayerController _playerController;
    private float _stunTimer = 0f;
    private float _length = 0f;
    private float _delta = 0;
    private float _pushForce;
    private Vector3 _pushDir;
    private Rigidbody _rb;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    public void Handle(PlayerController playerController)
    {
        if (!_playerController)
        {
            _playerController = playerController;
        }
        _playerController.CurrentSpeed = 0f;
        _playerController.AnimController
            .SetBool(_playerController.StunedAnim, true);
        _playerController.PlayerState = PlayerStateType.STUN;
    }

    public void UpdateState()
    {
        if (!_playerController) { return; }
        _stunTimer -= Time.deltaTime;
        if (_stunTimer <= 0)
        {
            _playerController.IsStunded = false;
            _playerController.Idle();
            return;
        }
        _pushForce = _pushForce - Time.deltaTime * _delta;
        _pushForce = _pushForce < 0 ? 0 : _pushForce;
    }

    public void Stuned(Vector3 velocityF, float duration)
    {
        _rb.velocity = velocityF;
        _pushForce = velocityF.magnitude;
        _pushDir = Vector3.Normalize(velocityF);
        _length = velocityF.magnitude;
        _stunTimer = duration;
        _delta = _length / duration;
    }

    public void FixedUpdateState()
    {
        _rb.velocity = _pushForce * _pushDir;
        Debug.DrawLine(transform.position, _pushForce * _pushDir, Color.red, 1f);
        _rb.AddForce(Physics.gravity);
    }

}
