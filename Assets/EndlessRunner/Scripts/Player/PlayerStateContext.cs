public class PlayerStateContext
{
    public IPlayerState CurrentState
    {
        get; set;
    }

    private readonly PlayerController _playerController;

    public PlayerStateContext(PlayerController playerController)
    {
        _playerController = playerController;
    }

    public void Transition()
    {
        _playerController.ClearAnim();
        CurrentState.Handle(_playerController);
    }

    public void Transition(IPlayerState state)
    {
        _playerController.ClearAnim();
        CurrentState = state;
        CurrentState.Handle(_playerController);
    }

    public void Update()
    {
        CurrentState.UpdateState();
    }

    public void FixedUpdate()
    {
        CurrentState.FixedUpdateState();
    }
}