using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(PlayerIdleState))]
[RequireComponent(typeof(PlayerMoveState))]
[RequireComponent(typeof(PlayerJumpState))]
[RequireComponent(typeof(PlayerStunState))]
[RequireComponent(typeof(Animator))]
public class PlayerController : NetworkBehaviour
{
    public readonly int IsGroundedAnim = Animator.StringToHash("IsGrounded");
    public readonly int MoveAnim = Animator.StringToHash("Move");
    public readonly int JumpAnim = Animator.StringToHash("Jump");
    public readonly int IdleAnim = Animator.StringToHash("Idle");
    public readonly int StunedAnim = Animator.StringToHash("Stuned");
    private Vector3 _startPosition = Vector3.zero;
    private readonly float FALLENDOWNBORDER = -0.5f;
    private float _fallenDownValue = -99999f;

    public float MaxSpeed { get; private set; } = 2.0f;

    public float CurrentSpeed { get; set; }

    private IPlayerState _idleState, _movingState, _jumpingState, _stunedState;

    public PlayerInput PlayerInput { get; private set; }

    private PlayerStateContext _playerSateContext;

    private Animator _animController;

    public bool IsGrounded { get; set; } = true;
    public bool IsStunded { get; set; } = false;

    public Animator AnimController {
        get { return _animController; }
        private set { _animController = value; }
    }

    private Rigidbody _rb;

    public PlayerStateType PlayerState { get; set; } = PlayerStateType.IDLE;

    private Transform _mainCamera;

    private readonly NetworkVariable<int> _points = new(0
      , NetworkVariableReadPermission.Everyone
      , NetworkVariableWritePermission.Owner);

    private readonly NetworkVariable<FixedString32Bytes> _name = new(""
      , NetworkVariableReadPermission.Everyone
      , NetworkVariableWritePermission.Owner);

    public
        override void OnNetworkSpawn()
    {
        _points.OnValueChanged += (int previousValue, int newValue) =>
        {
            GameUI.Instance.UpdateRankingPoints(_name.Value.ToString(), newValue);
        };

        _name.OnValueChanged += (FixedString32Bytes previousValue,
            FixedString32Bytes newValue) =>
        {
            GameUI.Instance.UpdateRankingName(previousValue.ToString()
                , newValue.ToString());
        };

    }

    private void Start()
    {
        ToggleMeshes(false);
        if (!IsOwner) { return; }
        GameManager.Instance.Player = gameObject;

        _name.Value = EditPlayerName.Instance.GetPlayerName();
        _rb = GetComponent<Rigidbody>();
        _playerSateContext = new PlayerStateContext(this);
        _idleState = GetComponent<PlayerIdleState>();
        _movingState = GetComponent<PlayerMoveState>();
        _jumpingState = GetComponent<PlayerJumpState>();
        _stunedState = GetComponent<PlayerStunState>();
        PlayerInput = GetComponent<PlayerInput>();
        _animController = GetComponent<Animator>();
        _mainCamera = Camera.main.transform;

        _playerSateContext.Transition(_idleState);
    }

    public void ToggleMeshes( bool active )
    {
        GetComponentsInChildren<SkinnedMeshRenderer>().ToList().ForEach(
            smr => smr.enabled = active);
    }

    private void Update()
    {
        if (!IsOwner) { return; }

        if(transform.position.y < _fallenDownValue)
        {
            transform.position = _startPosition;
            _points.Value -= 1;
        }

        if (!IsStunded)
        {
            Vector2 inputVector = PlayerInput.GetPlayerInput();
            if (inputVector == Vector2.zero)
            {
                if (IsGrounded)
                {
                    Idle();
                }
            }
            else
            {
                Moving();
            }
            SetPlayerPosition(inputVector);
        }
        _playerSateContext.Update();
    }

    public void Idle()
    {
        if (!IsOwner) { return; }
        if (PlayerState == PlayerStateType.IDLE) { return; }
        _playerSateContext.Transition(_idleState);
    }

    public void Moving()
    {
        if (!IsOwner) { return; }
        if ( PlayerState == PlayerStateType.MOVE
            ||!IsGrounded
            || IsStunded
            ) { return; }
        _playerSateContext.Transition(_movingState);
    }

    public void Jumping()
    {
        if (!IsOwner) { return; }
        if (!IsGrounded || IsStunded) { return; }
        _playerSateContext.Transition(_jumpingState);
    }

    public void Stuned(Vector3 value, float duration)
    {
        if (!IsOwner) { return; }
        IsStunded = true;
        GetComponent<PlayerStunState>().Stuned(value, duration);
        _playerSateContext.Transition(_stunedState);
    }

    public void SetPlayerPosition(Vector2 input)
    {
        if (!IsOwner) { return; }
        Vector3 cameraForward = new(_mainCamera.forward.x, 0, _mainCamera.forward.z);
        Vector3 cameraRight = new(_mainCamera.right.x, 0, _mainCamera.right.z);
        Vector3 moveDirection = cameraForward.normalized * input.y + cameraRight.normalized * input.x;
        Vector3 newPos = transform.position;
        newPos += PlayerInput.Speed * Time.deltaTime
            * moveDirection;
        transform.LookAt(newPos);
        transform.position = newPos;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!IsOwner) { return; }
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            Obstacle collisionObj = collision.gameObject.GetComponent<Obstacle>();
            foreach (ContactPoint contact in collision.contacts)
            {
                Vector3 hitDir = contact.normal;
                GetHit(hitDir * collisionObj.Force, collisionObj.StunTime);
                return;
            }

        }
        else if (collision.gameObject.CompareTag("Ground"))
        {
            IsGrounded = true;
            AnimController.SetBool(IsGroundedAnim, IsGrounded);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (!IsOwner) { return; }
        if (!collision.gameObject.CompareTag("Ground")) { return; }
        IsGrounded = false;
        AnimController.SetBool(IsGroundedAnim, IsGrounded);
    }

    public void ClearAnim()
    {
        if (!IsOwner) { return; }
        AnimController.SetBool(IdleAnim, false);
        AnimController.SetBool(JumpAnim, false);
        AnimController.SetBool(MoveAnim, false);
        AnimController.SetBool(StunedAnim, false);
    }

    private void FixedUpdate()
    {
        if (!IsOwner) { return; }
        _playerSateContext.FixedUpdate();
    }

    public void GetHit(Vector3 velocityF, float time)
    {
        if (!IsOwner) { return; }
        _rb.velocity = velocityF;
        //Debug.DrawLine(transform.position, velocityF, Color.red, 5f);
        Stuned(velocityF, time);
    }

    public void SetStartPositionAndValues(Vector3 newPos)
    {
        if (!IsOwner) { return; }
        transform.position = newPos;
        _startPosition = newPos;
        _fallenDownValue = _startPosition.y + FALLENDOWNBORDER;
        ToggleMeshesServerRpc();
        _rb.useGravity = true;
    }

    [ServerRpc]
    private void ToggleMeshesServerRpc()
    {
        ToggleMeshesClientRpc();
    }

    [ClientRpc]
    private void ToggleMeshesClientRpc()
    {
        ToggleMeshes(true);
    }

}
