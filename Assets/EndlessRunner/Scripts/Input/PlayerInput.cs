using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.HID;

public class PlayerInput : NetworkBehaviour
{
    private PlayerController _playerController;

    private PlayerInputActions _playerInputActions;

    [field: SerializeField] public float Speed { get; private set; } = 10f;

    public PlayerInputActions PlayerInputActions
    {
        get { return _playerInputActions; }
        private set { _playerInputActions = PlayerInputActions; }
    }

    private void Start()
    {
        if (!IsOwner) { return; }
        _playerInputActions = new PlayerInputActions();
        _playerInputActions.Player.Enable();
        _playerInputActions.Player.Jump.performed += Jump;
        _playerController = GetComponent<PlayerController>();
    }

    public Vector2 GetPlayerInput()
    {
        return PlayerInputActions.Player.Movement.ReadValue<Vector2>();
    }

    public void Jump(InputAction.CallbackContext context)
    {
        if (!_playerController) { return;  }
        _playerController.Jumping();
    }

}
