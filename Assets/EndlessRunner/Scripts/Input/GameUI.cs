using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class GameUI : MonoBehaviour
{
    public static GameUI Instance { get; private set; }

    private Dictionary<string, int> _ranking = new();
    [SerializeField] Transform _rankingElement;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        LobbyManager.Instance.OnGameStarted += LobbyManager_OnGameStarted;
        Hide();
    }

    public void UpdateRankingPoints(string name, int newValue)
    {
        if (_ranking.ContainsKey(name))
        {
            _ranking[name] = newValue;
        }
        else
        {
            _ranking.Add(name, newValue);
        }
        UpdateRankingList();
    }

    public void UpdateRankingName(string oldName, string newName)
    {
        if (_ranking.TryGetValue(oldName, out int value))
        {
            _ranking[newName] = value;
            _ranking.Remove(oldName);
        }
        else
        {
            _ranking.Add(newName, 0);
        }
        UpdateRankingList();
    }

    private void LobbyManager_OnGameStarted(object sender, System.EventArgs e)
    {
        Show();
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }

    private void UpdateRankingList()
    {
        if(_rankingElement.childCount < _ranking.Count)
        {
            for(int i = _rankingElement.childCount; i < _ranking.Count; ++i)
            {
                GameObject textGO = new();
                textGO.AddComponent<TextMeshProUGUI>();
                textGO.transform.SetParent(_rankingElement);
            }
        }

        int childCount = 0;
        foreach (var pair in _ranking.OrderByDescending(e => e.Value).ToList())
        {
            if(_rankingElement.GetChild(childCount).TryGetComponent(out TMP_Text text))
            {
                text.text = pair.Key + ": " + pair.Value;
            }
            ++childCount;
        }
    }

}
