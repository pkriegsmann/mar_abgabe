using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager Instance { get; private set; }

    [SerializeField] GameObject[] obstaclePrefab;
    private Vector3 spawnPos;
    private readonly float _repeatDelay = 3f;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public void SetPosition(Vector3 newPos)
    {
        spawnPos = newPos;
        StartCoroutine(Wait());
    }

    private void SpawnObstacle()
    {
        if(obstaclePrefab.Length == 0) { return; }
        int obstacleIndex = Random.Range(0, obstaclePrefab.Length);
        //x between -0.5 and 0.5
        spawnPos.x = Random.Range(-0.5f, 0.5f);
        //Instantiate(obstaclePrefab[obstacleIndex], spawnPos,
        //        obstaclePrefab[obstacleIndex].transform.rotation);
        //GameObject go = Instantiate(obstaclePrefab[obstacleIndex], spawnPos,
        //        obstaclePrefab[obstacleIndex].transform.rotation);
        //go.GetComponent<NetworkObject>().Spawn();
        SpawnObjectServerRpc();
    }

    [ServerRpc]
    private void SpawnObjectServerRpc()
    {
        int obstacleIndex = Random.Range(0, obstaclePrefab.Length);
        //x between -0.5 and 0.5
        spawnPos.x = Random.Range(-0.5f, 0.5f);
        GameObject go = Instantiate(obstaclePrefab[obstacleIndex], spawnPos,
                obstaclePrefab[obstacleIndex].transform.rotation);
        go.GetComponent<NetworkObject>().Spawn(true);
    }


    IEnumerator Wait()
    {
        for (; ; )
        {
            //SpawnObstacle();
            SpawnObjectServerRpc();
            yield return new WaitForSeconds(_repeatDelay);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(spawnPos, 0.3f);
    }
}
