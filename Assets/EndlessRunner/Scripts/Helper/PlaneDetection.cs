using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PlaneDetection : MonoBehaviour
{
    private ARPlaneManager _planeManager;

    private void Awake()
    {
        _planeManager = GetComponent<ARPlaneManager>();
        _planeManager.planesChanged += OnPlaneAdded;
    }

    private void OnPlaneAdded(ARPlanesChangedEventArgs eventArgs)
    {
        foreach (ARPlane plane in eventArgs.added)
        {
            if (plane.alignment == PlaneAlignment.HorizontalUp)
            {
                plane.gameObject.tag = "Ground";
            }
            else if (plane.alignment == PlaneAlignment.Vertical)
            {
                print("Vertical");
            }
        }
    }
}
