using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    public string Id { get; private set; }

    private void Awake()
    {
        Id = Guid.NewGuid().ToString();
    }
    // Start is called before the first frame update
    void Start()
    {

        foreach( DontDestroy i in FindObjectsOfType<DontDestroy>() ){
            if ( i != this && i.Id == Id )
            {
                Destroy(gameObject);
            }
        }

        DontDestroyOnLoad(gameObject);
    }

}
