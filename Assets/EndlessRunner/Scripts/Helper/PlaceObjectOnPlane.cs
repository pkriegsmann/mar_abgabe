using System;
using System.Collections;
using System.Collections.Generic;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public enum ObjectType
{
    PLAYER = 0,
    OBSTACLESPAWNER = 1,
    ZEROPOINT = 2,
}

public class PlaceObjectOnPlane : MonoBehaviour
{

    [SerializeField] GameObject placedPrefab;
    //GameObject _spawnedObject;
    private ARRaycastManager _raycastManager;
    private ARPlaneManager _planeManager;

    readonly List<ARRaycastHit> _hits = new();
    [SerializeField] GameObject visualObject;
    [SerializeField] GameObject ZeroPointButton;
    [SerializeField] GameObject SetPlayerButton;
    [SerializeField] GameObject ObstacleSpawnerButton;
    [SerializeField] Transform AdditionalOffset;
    private GameObject _zeroPointSphere;


    /// <summary>
    /// The prefab to instantiate on touch.
    /// </summary>
    public GameObject PlacedPrefab
    {
        get { return placedPrefab; }
        set { placedPrefab = value; }
    }

    /// <summary>
    /// The object instantiated as a result of a successful raycast intersection with a plane.
    /// </summary>
    public GameObject SpawnedObject { get; private set; }

    private void Start()
    {
        _raycastManager = GetComponent<ARRaycastManager>();
        _planeManager = GetComponent<ARPlaneManager>();
    }

    public void OnPlaceObject(int enumValue)
    {
        if (_raycastManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2),
            _hits, TrackableType.PlaneWithinPolygon))
        {
            
            if(!_hits[0].trackable.TryGetComponent<Transform>(out var hitTransform)) { return; }

            if(!hitTransform.gameObject.CompareTag("Ground")) { return; }

            switch((ObjectType)enumValue)
            {
                case ObjectType.PLAYER:

                    {
                        if( GameManager.Instance.Player.TryGetComponent(
                        out PlayerController playerController))
                        {
                            playerController.SetStartPositionAndValues(_hits[0].pose.position);
                            SetPlayerButton.SetActive(false);
                            ZeroPointButton.SetActive(false);
                            Destroy(_zeroPointSphere);
                            //only the host can set the obstacle spawner
                            if (playerController.IsOwnedByServer)
                            {
                                ObstacleSpawnerButton.SetActive(true);
                                return;
                            }
                            DiableVisualAndRaycasts();
                        }
                        break;
                    }
                case ObjectType.OBSTACLESPAWNER:
                    {
                        SpawnManager.Instance.SetPosition(_hits[0].pose.position);
                        ObstacleSpawnerButton.SetActive(false);
                        DiableVisualAndRaycasts();
                        break;
                    }
                case ObjectType.ZEROPOINT:
                    {
                        AdditionalOffset.localPosition = AdditionalOffset.localPosition - _hits[0].pose.position;
                        if(_zeroPointSphere == null)
                        {
                            _zeroPointSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                            _zeroPointSphere.transform.localScale = new(0.05f, 0.05f, 0.05f);
                            _zeroPointSphere.transform.position = Vector3.zero;
                        }
                        break;
                    }
            }
        }
    }

    public void DiableVisualAndRaycasts()
    {
        visualObject.SetActive(false);
        _raycastManager.enabled = false;
        _planeManager.enabled = false;

        //shader invisable
        GameObject trackables = GameObject.Find("Trackables");
        foreach (Transform plane in trackables.transform)
        {
            var renderer = plane.gameObject.GetComponent<MeshRenderer>();
            renderer.material.SetColor("_TexTintColor", new Color(1, 1, 1, 0));
        }
    }
}
