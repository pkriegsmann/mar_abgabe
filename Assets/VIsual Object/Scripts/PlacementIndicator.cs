using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PlacementIndicator : MonoBehaviour
{
    private ARRaycastManager rayManager;
    private GameObject _visual;

    void Start()
    {
        // get the components
        rayManager = FindObjectOfType<ARRaycastManager>();
        _visual = transform.GetChild(0).gameObject;

        // hide the placement indicator visual
        _visual.SetActive(false);
    }

    void Update()
    {
        // shoot a raycast from the center of the screen
        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        rayManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), hits, TrackableType.Planes);

        // if we hit an AR plane surface, update the position and rotation
        if (hits.Count > 0)
        {
            transform.SetPositionAndRotation(hits[0].pose.position
                , hits[0].pose.rotation);

            // enable the visual if it's disabled
            if (!_visual.activeInHierarchy)
                _visual.SetActive(true);
        }
    }
}